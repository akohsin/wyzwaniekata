package pl.sda.tdd.kata;

public class Kalkulator {

    /**
     * Kalkulator
     *
     * @param liczby - liczby oddzielone przecinkiem, może przyjmować od 0 do 2 liczb oddzielonych przecinkiem
     * @return - zero for empty String
     */
//1,2,5\n5->8
    public static int add(String liczby) {
        if (liczby.equals("")||liczby.equals(" "))throw new IllegalArgumentException("Podaj niezerowa ilosc liczb");
        String[] pojLiczby = new String[0];
        if (liczby.startsWith("//"))
        {
            char sep = liczby.charAt(2);
            liczby=liczby.substring(3);
            String separator="("+ sep+"|\n|,)";
            pojLiczby = liczby.trim().split(separator);
        }
        else if (liczby.contains("\n") || liczby.contains(",")) {

            pojLiczby = liczby.trim().split("(,|\n)");
        }
//        else pojLiczby = liczby.split("\n");
//        if (pojLiczby.length==1) {
//            throw new IllegalArgumentException("Podaj niezerowa ilosc liczb");
//        }
        int suma = 0;
        for (int i = 0; i < pojLiczby.length; i++) {

         if (!pojLiczby[i].equals(null))suma+= Integer.parseInt(pojLiczby[i]);
        }
        return suma;
    }
}
