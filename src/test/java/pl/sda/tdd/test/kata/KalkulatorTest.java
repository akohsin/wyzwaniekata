package pl.sda.tdd.test.kata;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.sda.tdd.kata.Kalkulator;

import static org.junit.Assert.assertEquals;

public class KalkulatorTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test

    public void dlaPustegoStringaRzucaWyjatek() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Podaj niezerowa ilosc liczb");
        Kalkulator.add("");
    }

    @Test
    public void dlaSpacjiRzucaWyjatek() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Podaj niezerowa ilosc liczb");
        Kalkulator.add(" ");
    }

    @Test

    public void dlaDwóchliczboddzielonychPrzecinkiemZwracaWynikLiczbowy() {
        assertEquals(3, Kalkulator.add("1,2"));
    }

    @Test
    @Ignore
    public void dlaTrzechliczboddzielonychPrzecinkiemZwracaWyjatek() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Nieprawidłowe dane wejściowe");
        Kalkulator.add("1,2,3");

    }

    @Test
    public void piecplusstoplusdwadziesciapiecdajestodwadziesciapiec() {
        assertEquals(130, Kalkulator.add("5,100,25"));
    }

    @Test
    public void sumowanieLiczbZeZnakamiNowejLinii() {

        assertEquals(100, Kalkulator.add("10\n90"));
    }

    @Test
    public void dodajeStringZZadanymSeparatorem(){
        assertEquals(100,Kalkulator.add("//#\n45#55"));
    }


}
